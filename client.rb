#!/usr/bin/ruby

require 'libvirt'
require 'fileutils'
require 'uuidtools'

@TEMP_DIR = "/tmp/goosefarm"
@TEMPLATES = "#{File.expand_path(File.dirname(__FILE__))}/templates"
@LIVE_IMAGES = "/var/lib/libvirt/images"
@MASTER_IMAGES = "/var/lib/libvirt/masters"

puts('')
puts(' .d88b.  .d88b.  .d88b. .d8888b  .d88b.')
puts('d88P"88bd88""88bd88""88b88K     d8P  Y8b')
puts('888  888888  888888  888"Y8888b.88888888')
puts('Y88b 888Y88..88PY88..88P     X88Y8b.')
puts(' "Y88888 "Y88P"  "Y88P"  88888P\' "Y8888')
puts('     888       __')
puts('Y8b d88P      / _| ')
puts(' "Y88P"      | |_ __ _ _ __ _ __ ___')
puts('             |  _/ _` | \'__| \'_ ` _ \\')
puts('             | || (_| | |  | | | | | |')
puts('             |_| \__,_|_|  |_| |_| |_|')
puts('')

puts('Starting up...')

@hv = Libvirt::open("qemu:///system")

FileUtils.mkdir_p(@TEMP_DIR)

def destroy_vm(name)
  begin
    dom = @hv.lookup_domain_by_name(name)
  rescue => e
    puts("ERROR: Unable to destroy '#{name}': #{e}")
    return
  end
  machine_uuid = dom.uuid
  begin
    if dom.active?
      dom.destroy
    end
    dom.undefine
  rescue => e
    puts("ERROR: Unable to destroy '#{name}': #{e}")
  end

  image = "#{@LIVE_IMAGES}/#{name}-#{machine_uuid}.qcow2"
  puts("Cleaning up #{image}...")
  FileUtils.rm(image)

  puts("#{name} was destroyed.")
end

def create_vm(name, cpus, memory, os_image)
  random_id = UUIDTools::UUID.random_create
  begin
    dom = @hv.lookup_domain_by_name(name)

    if dom.active?
      puts("WARNING: '#{name}' already exists and is running!")
      return dom.uuid
    else
      puts("WARNING: '#{name}' already exists and is not running.  Cleaning up..")
      destroy_vm(name)
    end
  rescue
    puts("Provisioning #{name} (#{random_id})...")
  end

  memory_kib = memory * 1024
  source_image = "#{@MASTER_IMAGES}/#{os_image}"
  destination_image = "#{@LIVE_IMAGES}/#{name}-#{random_id}.qcow2"
  if !File.file?(source_image)
    puts("ERROR: '#{source_image}' doesn't exist!")
    return nil
  end

  # Generate a cloud-config
  cloud_config_user = File.read("#{@TEMPLATES}/config-base.yaml")
  cloud_config_user = cloud_config_user.gsub(/%%NAME%%/, name)
  File.open("#{@TEMP_DIR}/user_data_#{random_id}.yaml", 'w') { |file| file.write(cloud_config_user) }
  `cloud-localds #{@TEMP_DIR}/#{random_id}.iso #{@TEMP_DIR}/user_data_#{random_id}.yaml`

  FileUtils.cp(source_image, destination_image)
  puts("Generating domain configuration #{@TEMP_DIR}/#{random_id}.xml...")
  new_domain_xml = File.read("#{@TEMPLATES}/domain.xml")
  new_domain_xml = new_domain_xml.gsub(/%%NAME%%/, name)
  new_domain_xml = new_domain_xml.gsub(/%%UUID%%/, random_id)
  new_domain_xml = new_domain_xml.gsub(/%%MEMORY_KIB%%/, memory_kib.to_s)
  new_domain_xml = new_domain_xml.gsub(/%%INIT_ISO%%/, "#{@TEMP_DIR}/#{random_id}.iso")
  new_domain_xml = new_domain_xml.gsub(/%%MAC_UNIQUE%%/, "#{Random.rand(10..99)}:#{Random.rand(10..99)}:#{Random.rand(10..99)}")
  new_domain_xml = new_domain_xml.gsub(/%%CPU_COUNT%%/, cpus.to_s)
  new_domain_xml = new_domain_xml.gsub(/%%DISK_IMAGE%%/, destination_image)
  puts("Attempting to boot new domain #{name} (#{random_id})")
  begin
    dom = @hv.define_domain_xml(new_domain_xml)
    dom.create
  rescue => e
    puts("ERROR: Failed to start new vm: '#{e}'")
    return nil
  end
  return random_id
end


# Main process logic
new_vm = create_vm("test", 2, 512, "haiku_gcc2.qcow2") 
if new_vm == nil
  puts("Error making new vm!")
  destroy_vm("test")
else
  puts("New vm #{new_vm} created!")
end
sleep 10
destroy_vm("test")
